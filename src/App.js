import React, { Component } from "react";
import { Route, Link, Switch, withRouter } from "react-router-dom";
import Home from "./pages/home";
import { Grid, User as UsersIcon, Menu as MenuBars, User, Info} from "react-feather";
import Login from "./pages/login";

import _403 from "./img/403.png";
import _500 from "./img/500.jpg";
import {
  progressBarFetch,
  setOriginalFetch,
  ProgressBar
} from "react-fetch-progressbar";
import Access from "./components/accessManager";
import Menu from "./components/Menu";
import config from "./config.js";
import Alert from "./components/alert";
import stamp from "./img/logo.png";
import ProfileView from "./pages/administration/adminView";
import Books from "./pages/book/books";
import Comments from "./pages/comment/comments";
import Characters from "./pages/character/characters";
import Search from "./pages/character/search";


// Configs
window.server = config.server_url;

// create a fetch that will no trigger the progreebar in the background
window.silentFetch = fetch;

// check token validity
let checkTokenValidity = () => {
  window
    .silentFetch(`${window.server}/refresh-token`, {
      headers: {
        Authorization: localStorage.token
      }
    })
    .then(response => response.json())
    .then(response => {
      // console.log(response);
      if (response.status === 401) {
        window.logout(true);
        setTimeout(() => {
          alert("Your Session has expired");
        }, 0);
      } else {
        setTimeout(() => {
          // console.log("checking validity");
          if (localStorage.token) {
            checkTokenValidity();
          }
        }, 30000);
      }
    })
    .catch(d => {
      console.log(d);
    });
};

window.logout = force => {
  let logout = () => {
    let path = window.location.pathname;
    localStorage.clear();
    localStorage.previousPath = path;
    window.location = "/login";
  };

  if (force) {
    logout();
    return false;
  }

  window.alert2.confirm({
    icon: <Info size={70}></Info>,
    title: "Are you sure that you want to Logout ?",
    message: "If you agree your session will be terminated",
    confirmText: "Log Out",
    onSubmit: () => {
      logout();
    }
  });

  // window.alert2.onConfirm = () => {
  //   logout();
  // };

  // let path = window.location.pathname;
  // localStorage.clear();
  // localStorage.previousPath = path;
  // window.location = "/login";
};

// set Progress Bar
setOriginalFetch(window.fetch);
window.standardFetch = progressBarFetch;

// set app globals

// end Configs

// check if logged in
if (localStorage.token && localStorage.user) {
  window.user = JSON.parse(localStorage.user);
  //console.log(window.user.user);
} else {
}

class App extends Component {
  state = { loaded: false, status: 0, errorload: false, blur: false };

  render() {
    return (
      <>
        <div className={!this.state.blur || "blur-alert "}>
          <Switch>
            <Route path="/login" component={Login} />
            {(this.state.loaded || !localStorage.token) && (
              <Route path="/" component={Portal} />
            )}
            {this.getStatus(this.state.status, this.state.errorload)}
          </Switch>
        </div>
        <Alert
          onRef={ref => {
            window.alert2 = ref;
          }}
          toggle={blur => {
            this.setState({ blur });
          }}
        ></Alert>
      </>
    );
  }

  componentDidMount = () => {

    let roles = {};

    roles = { CHARACTER_READ: [1], CHARACTER_CREATE: [1] };

    window.roles = roles;
 
    this.checkStatus();
  };

  // checkStatus = () => {
    
  //     this.setState({ loaded: true, errorload: true });
  // };

  checkStatus = () => {
    if (window.roles)
      this.setState({ loaded: true, errorload: true });
  };

  get = (url, callback) => {
    fetch(`${window.server}/${url}`, {
      headers: {
        Authorization: localStorage.token
      }
    })
      .then(response => response.json())
      .then(response => {
        // console.log(response);
        if (response.code === 401) {
          localStorage.clear();
          this.props.history.push("/login");
        }
        if (response.code) {
          this.setState({ status: response.code, errorload: true });
        }
        callback(response);
        this.checkStatus();
      })
      .catch(d => {
        console.log(d);
      });
  };

  inactivityTime = function() {
    let time;
    let logout = () => {
      if (localStorage.token) {
        localStorage.clear();
        window.location = "/login";
      }
    };

    let resetTimer = () => {
      clearTimeout(time);
      time = setTimeout(window.logout, 9000);
    };

    window.onload = resetTimer;
    // DOM Events
    document.onmousemove = resetTimer;
    document.onkeypress = resetTimer;
  };

  getStatus = () => {
    if (!this.state.errorload)
      return (
        <div className="loader h-100 w-100 d-flex flex-row align-items-center justify-content-center show-loader">
          <div className="lds-roller">
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
          </div>
        </div>
      );

    if (this.state.status !== 0 && this.state.status !== 0) {
      if (this.state.status === 403) {
        return (
          <div className="loader h-100 w-100 d-flex flex-row align-items-center justify-content-center show-loader">
            <img src={_403} alt="" />
          </div>
        );
      } else {
        return (
          <div className="loader h-100 w-100 d-flex flex-row align-items-center justify-content-center show-loader">
            <img src={_500} alt="" />
          </div>
        );
      }
    }
  };
}

class Portal extends Component {
  state = { showMenu: false };
  render() {
    if (typeof localStorage.token === "undefined") {
      this.props.history.push("/login");
    }
    return (
      <>
        <div id="wrapper" className="pt-5 mt-md-0 pt-md-0 mt-2">
          <Menu></Menu>
          <div id="content-wrapper" className="d-flex flex-column bg-light">
            <div className="preloader-container w-100">
              <ProgressBar />
            </div>
            <div className="d-flex flex-fill flex-column">
            <Route path="/" exact component={Home} />{" "}
            <Route
                path="/ProfileView/:path"
                component={ProfileView}
              />{" "}
              <Route path="/profile" exact component={ProfileView} />{" "}
              <Route path="/books" exact component={Books} />{" "}
              <Route path="/all-comments" exact component={Comments} />{" "}
              <Route path="/all-characters" exact component={Characters} />{" "}
              <Route path="/search-characters" exact component={Search} />{" "}
            </div>
            <img src={stamp} className="stamp" alt="" />
          </div>
        </div>
      </>
    );
  }
  componentDidMount() {
    if (localStorage.token) {
      checkTokenValidity();
    }
  }
}

window.verifyNumber = n => {
  n = n + "";
  if (n[0] + n[1] + n[2] === "254") {
    return parseInt(n);
  } else {
    return parseInt("254" + parseInt(n));
  }
};

window.fetch = (url, options) => {
  return new Promise((resolve, reject) => {
    window
      .standardFetch(url, options)
      .then(response => {
        resolve(response);
        return response.clone().json();
      })
      .then(response => {
        // console.log(response);
        // response.json = () => {
        //   return response;
        // };
        // resolve(response);

        if (response.code === 401 && localStorage.token) {
          window.alert2.notify({
            title: "Your Session has Expired",
            description: "Please Login once more to continue using the system",
            color: "material-deep-orange",
            autoClose: false,
            onClose: () => {
              window.logout(true);
            }
          });
        }
      })
      .catch(error => {
        // window.alert2.notify("Your session has expired");
        reject(error);

        // window.alert2.notify({
        //   title: "Connection error",
        //   description:
        //     "There seems like there's a connection problem, please try again later",
        //   color: "material-deep-orange",
        //   autoClose: false,
        //   onClose: () => {
        //     window.logout(true);
        //   }
        // });

        // console.log(error);
        reject(error);
      });
  });
};

export default withRouter(App);
