import React, { Component } from 'react';

class Access extends Component {
  permissions = window.roles;

  state = {
    authorized:
      window.user &&
      window.user.role &&
      ((this.permissions[this.props.permission] &&
        this.permissions[this.props.permission]?.indexOf(
          window.user.role
        ) !== -1) ||
        this.props.permission === 'all')
  };

  render() {
    return this.state.authorized ? <>{this.props.children}</> : <></>;
  }
}

export default Access;
