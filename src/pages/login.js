import React, { Component } from 'react';
import logo from '../img/logo.png';
import { User, Lock, Mail } from 'react-feather';
import { Link } from 'react-router-dom';
import Modal from '../components/modal';
import LButton from '../components/loadingButton';

class Login extends Component {
  state = {
    custom: true,
    loading: false,
    email: '',
    password: '',
    verifyStatus: 0,
    loginStatus: 0
  };
  render() {
    return (
      <div className='d-flex flex-fill flex-column main-cover justify-content-center wallpaper'>
        <div className='container'>
          <div className='row justify-content-center'>
            <div className='col-xl-10 col-lg-12 col-md-9'>
              <div className='card o-hidden border-0 shadow-lg my-5 login-card'>
                <div className='card-body p-0'>
                  <div className='row'>
                    <div className='col-lg-6 bg-light d-none default-bg align-items-center justify-content-center flex-row d-md-flex icon-holder'>
                      <img src={logo} className='main-logo' alt='' />
                    </div>
                    <div className='col-lg-6'>
                      <div className='p-md-5 p-4'>
                        <div className='text-center'>
                          <h1 className='h4 text-gray-900 mb-4 font-weight-bold'>
                            Topup Mama
                          </h1>
                        </div>

                        <div className='text-center mb-4'>
                          <small className='text-muted'>Admin login</small>
                        </div>
                        <form
                          onSubmit={e => {
                            e.preventDefault();
                            this.login();
                          }}>
                          <div className='form-group d-flex flex-row align-items-center'>
                            <Mail
                              className='login-icon align-self-center position-absolute ml-2'
                              color='gray'
                              size={18}
                            />

                            <input
                              type='email'
                              className='form-control form-control-user  icon-input'
                              placeholder='Email Address'
                              value={this.state.email}
                              onChange={e => {
                                this.setState({ email: e.target.value });
                              }}
                            />
                          </div>
                          <div className='form-group d-flex flex-row align-items-center'>
                            <Lock
                              className='login-icon align-self-center position-absolute ml-2 '
                              color='gray'
                              size={18}></Lock>
                            <input
                              type='password'
                              className='form-control form-control-user icon-input'
                              placeholder='Password'
                              //autocomplete='off'
                              onChange={e => {
                                this.setState({ password: e.target.value });
                              }}
                              value={this.state.password}
                            />
                          </div>
                          <div className='form-group d-flex flex-row align-items-center justify-content-between'>
                            {/* <div className="custom-control custom-checkbox small">
                            <input
                              type="checkbox"
                              className="custom-control-input"
                              checked
                              autocomplete="false"
                            />
                            <label
                              className="custom-control-label"
                              for="customCheck">
                              Remember Me
                            </label>
                          </div> */}
                            {/* <Link
                              className=''
                              onClick={() =>
                                this.setState({ resetVisible: true })
                              }>
                              <small>Forgot password?</small>
                            </Link> */}
                          </div>
                          <div className='text-center'>
                            <div className='d-inline-block'>
                              {/* <button
                              disabled={this.state.loading}
                              className="btn btn-primary btn-user btn-block default-bg d-flex flex-row align-items-center"
                              onClick={this.login}>
                              {this.state.loading && (
                                <div className="lds-dual-ring mr-3"></div>
                              )}
                              {!this.state.loading && (
                                <span className="mx-4">Login</span>
                              )}
                              {this.state.loading && (
                                <>
                                  <span>Loading ...</span>{" "}
                                  <div className="pl-3"></div>
                                </>
                              )}
                            </button> */}

                              <LButton
                                text='Login'
                                status={this.state.loginStatus}></LButton>
                            </div>
                          </div>
                        </form>
                        <hr />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }

  login = () => {
    let { email } = this.state;
    let { password } = this.state;
    if (!(email !== '' && password !== '')) {
      alert('Please fill in all the required values');
      return false;
    }
    
  let email2 = email.replace(/\s/g, "");
  let pass = password.replace(/\s/g, "");

    this.setState({ loginStatus: 1 });
    // let data = { name, email, password };

    fetch(window.server + '/login', {
      method: 'POST',
      headers: {
        // 'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      body: `{"email": "${email2}", "password": "${pass}"}`
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);

        // this.setState({ loading: false });

        if (response.status === 400 || response.status === 401) {
            alert(response.message);
            this.setState({ loginStatus: 0 });
            return false;
        } else if (response.data.access_token) {
          localStorage.token = response.data.access_token;
          window.user = response.data.user;
          localStorage.user = JSON.stringify(window.user);
          let location = '/';
          if (localStorage.previousPath) location = localStorage.previousPath;
          window.location = location;
          this.setState({ loginStatus: 2 });
          // this.props.history.push("/");
        }
      })
      .catch(() => {
        this.setState({ loading: false });
        alert('Something went wrong. Please try again.');
        this.setState({ loginStatus: 0 });
      });
  };

}

export default Login;
