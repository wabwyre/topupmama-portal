import React, { Component } from "react";
import Table from "../../components/Table";
import Filter from "../../components/filter";
import { Plus, Target, Circle, Smartphone } from "react-feather";
import { Link } from "react-router-dom";
import Modal from "../../components/modal";
import Form from "../../components/form";
import { Verify } from "crypto";
import Fuse from "fuse.js";
import moment from "moment";
import Nav from "../../components/Nav";

class Search extends Component {
  state = {
    searchValue: "",
    Search: [],
    tableData: { data: [] },
    response: { data: [] },
    tableError: false,
    query: {},
    filter: {},
    table_loading: false
  };

  timeout = null;
  render() {
    return (
      <div className="">
         <div className="mt-3 border-0 card shado mx-3 shadow">
            <div className="text-center">
              <h4 className="font-weight-bold">Search Characters</h4>
            </div>
            <div className="mt-3 align-items-center">
               <Form
              submitText={"Search"}
              back={false}
              inputs={[
                {
                  label: "Search by Gender",
                  name: "search",
                  type: "text",
                  value: this.state.searchValue
                }
              ]}
              submit={data => {
                //console.log(data);
                //this.setState({ details: data });
                setTimeout(() => {
                   this.handleSearch(data);
                  }, 0);
              }}
            />
            </div>

          </div>

         <div className="mt-3 table-card  border-0 card shado mx-3 shadow">
          <div className="p-4">
            <Table
              // search={["firstname", "middlename", "surname", "msisdn", "id_number"]}
            //   sort="id"
            //   sortDirection={-1}
              data={this.state.tableData}
              fetch={params => {
                this.setState({ query: params });
              }}
              loading={this.state.table_loading}
              fetchError={this.state.tableError}
            />
          </div>
        </div>
       
      </div>
    );
  }

  handleOnChange = e => {
    this.setState({ searchValue: e.target.value });
  };
  
  handleSearch = data => {
        console.log(data);
        let sdata = JSON.stringify(data);
        let obj = JSON.parse(sdata);
        let values = Object.values(obj);

        //alert(values);
        fetch(`${window.server}/character/search-characters/?search=${values}`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: localStorage.token
        }
          })
          .then(response => response.json())
          .then(response => {
            //console.log(response);
            let record = [];
            
             response.data.map((d) => {
              record.push({
                id: d.id,
                name: d.name,
                gender: d.gender,
                culture: d.culture,
                born: d.born,
                died: d.died,
                titles: JSON.parse(d.titles),
                aliases: JSON.parse(d.aliases),
                playedBy: JSON.parse(d.playedBy)
                // action: (
                //   <div className="d-flex flex-row">
                //     <Link
                //       to={"/Characters/details/" + d.id}
                //       className="btn btn-sm btn-primary px-3 btn-round"
                //     >
                //       View
                //     </Link>
                //   </div>
                // )
              });
            });
            
            response.data = record;
            //console.log(record);
            let dts = {};
              dts.data = record;

              this.setState({
                tableData: { ...response, ...dts },
                response,
                table_loading: false
              });

          })
          .catch(d => {
            this.setState({ tableError: true });
            //console.log(d);
          });
  };

  componentDidUpdate(prevProps, prevState) {
      let $t = this;

      clearTimeout(this.timeout);
      this.timeout = setTimeout(function() {
        $t.handleSearch();
      }, 100);
  }

}

export default Search;