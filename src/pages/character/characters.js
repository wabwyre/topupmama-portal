import React, { Component } from 'react';
import Table from '../../components/Table';
import { Link } from 'react-router-dom';
import Nav from '../../components/Nav';
import moment from 'moment';
import Filter from '../../components/filter';
import Access from '../../components/accessManager';
import Modal from '../../components/modal';
import { Plus, Target, Circle, Smartphone } from 'react-feather';
import ReactJson from 'react-json-view';

class Characters extends Component {
  state = {
    tableData: { data: [] },
    response: { data: [] },
    tableError: false,
    query: {},
    filter: {},
    table_loading: false,
  };
  timeout = null;
  render() {
    return (
      <div className=''>
        <Nav
          name='Characters'
         ></Nav>
        <div className='mt-3 table-card border-0 card shado mx-3 shadow'> 

               
          <div className='p-4'>
            <Table
              data={this.state.tableData}
              fetch={params => {
                this.setState({ query: params });
              }}
              loading={this.state.table_loading}
              fetchError={this.state.tableError}
            />
          </div>
          {this.state.json && (
            <Modal
              visible={true}
              close={() => {
                this.setState({ json: null });
              }}>
              <ReactJson
                displayDataTypes={false}
                displayObjectSize={false}
                src={this.state.json}
              />
            </Modal>
          )}
        </div>
      </div>
    );
  }

  fetchCharacters = () => {
    this.setState({ table_loading: true });

    // let q = {
    //   //...this.state.filter,
    //   ...this.state.query
    // };

    // let urlParams = Object.entries(q)
    //   .map(e => e.join("="))
    //   .join("&");
    // console.log(urlParams);
    fetch(`${window.server}/character/all`, {
      headers: {
        Authorization: localStorage.token
      }
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        let data = [];
        response.data.map((d, i) => {
          data.push({
            id: d.id,
            name: d.name,
            gender: d.gender,
            culture: d.culture,
            born: d.born,
            died: d.died,
            titles: JSON.parse(d.titles),
            aliases: JSON.parse(d.aliases),
            playedBy: JSON.parse(d.playedBy)
            // action: (
            //   <div className="d-flex flex-row">
            //     <Link
            //       to={"/Characters/details/" + d.id}
            //       className="btn btn-sm btn-primary px-3 btn-round"
            //     >
            //       View
            //     </Link>
            //   </div>
            // )
          });
        });
        let dts = {};
        dts.data = data;
        this.setState({
          tableData: { ...response, ...dts },
          response,
          table_loading: false
        });
      })
      .catch(d => {
        this.setState({ table_loading: false });
        console.error(d);
      });
  };


  componentDidUpdate(prevProps, prevState) {
    if (
        JSON.stringify({ ...this.state.query, ...this.state.filter }) !==
        JSON.stringify({ ...prevState.query, ...prevState.filter })
    ) {
        let $t = this;

        clearTimeout(this.timeout);
        this.timeout = setTimeout(function () {
            $t.fetchCharacters();
        }, 100);
    }
 }
}

export default Characters;