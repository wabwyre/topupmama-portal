import React, { Component } from "react";
import { Route, Link, Switch } from "react-router-dom";
// import Details from "./clients/details";
import {
  Plus,
  Edit,
  Check,
  UserPlus,
  AlertTriangle,
  ThumbsUp,
  RefreshCw,
  Edit2,
  UserX, XCircle, CheckCircle
} from "react-feather";
import Tabs from "../../components/tabs";
import Modal from "../../components/modal";
import Form from "../../components/form";
import Access from "../../components/accessManager";
import LButton from "../../components/loadingButton";
import Details from "./details";
import config from "../../config";

class PortalView extends Component {
  state = {
    currentRoute: "",
    editModal: false,
    loadingStatus: 0
  };
  render() {
    let user = [];

    return (
      <div className="bg-light">
        <div className="card table-card m-3">
          <div className="text-mute pt-3 pl-3">
            <small className="text-mute">Portal User > View</small>
          </div>

          <div className="profile p-3 d-md-flex flex-row align-items-center justify-content-between">
            <div className="d-md-flex flex-row align-items-center">
              <div className="border avatar-lg bg-light d-flex flex-row align-items-center justify-content-center">
                <span className="initials">
                  {this.state.name ? this.state.name[0] : ""}
                </span>
                <img
                  src={`${window.server}${this.state.passport_path &&
                    this.state.passport_path.replace("public", "")}`}
                  className="avatar"
                  alt=""
                />
              </div>
              <div className="ml-md-4 my-3 my-md-0">
                <h4 className="text-capitalize">
                  {this.state.name &&
                    (
                      this.state.name 
                    ).toLowerCase()}
                </h4>
                <div className="ml-2 mt-1">
                  <span className="badge badge-secondary px-1">Portal User</span>
                </div>
              </div>
            </div>

            {/* <Access permission='client_add'> */}
            <div className="d-md-flex flex-row align-items-center justify-content-center text-center">
              {this.state.id && (
                <div>
                  <Access permission="all">
                    <LButton
                      text={`${
                        this.state.status === 1 ? "Disable" : "Enable"
                      } User`}
                      className={
                        this.state.status === 1
                          ? "material-red"
                          : "material-green"
                      }
                      onClick={this.toggleStatus}
                      status={this.state.loadingStatus}
                    ></LButton>
                  </Access>
                </div>
              )}
               <div className=" ml-3">
                {this.state.id && (
                  <Access permission="all">
                    <button
                      onClick={() => {
                        this.setState({ editModal: true });
                        //console.log(this.state.response.data[0]);
                      }}
                      className="option-card no-wrap pr-3 d-md-flex d-inline-block my-2 flex-row btn align-items-center btn-primary btn-round mr-3"
                    >
                      <Edit size={18} />
                      <span className="pl-1 font-weight-bold">Edit Admin</span>
                    </button>
                  </Access>
                )}
              </div>

              <div className=" ml-3">
                {this.state.id && (
                   <Access permission="all">
                    <button 
                 onClick={this.resetPass}
                className="option-card no-wrap pr-3 d-md-flex d-inline-block my-2 flex-row btn align-items-center btn-primary btn-round mr-3">
                  <RefreshCw size={18} />
                  <span className="pl-1 font-weight-bold">Reset Password</span>
                </button>
                  </Access>
                )}
              </div>
             
            </div>
            {/* </Access> */}
          </div>

          <Tabs
            tabs={[
              {
                label: "DETAILS",
                link:
                  "/PortalView/details/" +
                  this.props.match.params.id,
                access: "all"
              }
            ]}
          ></Tabs>
        </div>
        <Route
          path="/PortalView/details/:id"
          exact
          component={Details}
        />

       <Modal
          visible={this.state.editModal}
          close={() => this.setState({ editModal: false })}
        >
          <div className="d-flex flex-row align-items-center">
            <UserPlus className="mr-3"></UserPlus>
            <h5 className="m-0 mt-1">Edit Portal User</h5>
          </div>

          {this.state.id && (
          <div className="mt-3">
            <Form
              inputs={[
                {
                  label: "Full name",
                  name: "name",
                  type: "text",
                  value: this.state.name
                },
                {
                  label: "Email",
                  name: "email",
                  type: "email",
                  value: this.state.email
                },
                {
                  label: "Phone Number",
                  name: "msisdn",
                  type: "number",
                  value: JSON.parse(this.state.addOnData).msisdn
                },
                {
                  label: "Access Role",
                  name: "group_id",
                  value: this.state.group_id,
                  type: "select",
                  options: window.groups.reverse().map(d => {
                    return { name: d.name, value: d.id };
                  })
                },
                {
                  label: "2 Factor Authentication",
                  name: "secondFactor_status",
                  value: this.state.secondFactor_status,
                  type: "select",
                  options: [
                    { name: "Enabled", value: "0" },
                    { name: "Disabled", value: "1" }
                  ]
                },
                {
                  label: "2 Factor Method",
                  name: "secondFactor_selected",
                  value: "SMS",
                  type: "text",
                  readonly: "readonly"
                }
              ]}
              submit={data => {
                console.log(data);
                this.setState({ details: data });
                setTimeout(() => {
                  if (this.verify(data)) {
                    this.editUser(data);
                  }
                }, 0);
              }}
            />
          </div>
           )}{" "}
        </Modal>

      </div>
    );
  }

  componentDidMount = () => {
    this.fetch();
  };

  fetch = () => {
    this.setState({ table_loading: true });

    fetch(`${window.server}/admins?id=${this.props.match.params.id}`, {
      headers: {
        Authorization: localStorage.token
      }
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        
        this.setState({
          ...response.data[0],
          table_loading: false
        });
      })
      .catch(d => {
        this.setState({ table_loading: false });
        console.error(d);
      });
  };

  verify(data) {
    let result = true;
    let missing = [];
    Object.keys(data).map(d => {
      if (!data[d] || data[d] === "") {
        missing.push(d.replace("_id", "").replace(/_/g, " "));
        result = false;
      }
    });
    missing.join(", ");
    if (!result) alert("Please fill all the require fields : " + missing);
    return result;
  }

  toggleStatus = () => {
    if (
      !window.confirm(
        `Are you sure that you want to ${
          this.state.status === 1 ? "Disable" : "Enable"
        } this user ?`
      )
    )
      return false;

    this.setState({ modalVisible: true });
    fetch(`${window.server}/admins/${this.props.match.params.id}`, {
      method: "PATCH",
      headers: {
        Authorization: localStorage.token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ status: this.state.status === 1 ? 0 : 1 })
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.code) {
          alert(
            response.message +
              " \n " +
              (response.errors[0] ? response.errors[0].message : "")
          );
          this.setState({ modalVisible: false });
        } else {
          alert("Updated successfully");
          console.log(response);
          window.location.reload();
        }
      })
      .catch(d => {
        console.log("Error saving the data");
        console.log(d);
        this.setState({ modalVisible: false });
      });
  };

  unblock = data => {
    fetch(`${window.server}/admins/${this.props.match.params.id}`, {
        method: "PATCH",
        headers: {
          Authorization: localStorage.token,
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      })
        .then(response => response.json())
        .then(response => {
          console.log(response);
          if (response.code) {
            alert(
              response.message +
                " \n " +
                (response.errors[0] ? response.errors[0].message : "")
            );
            this.setState({ modalVisible: false });
          } else {
            console.log(response);
  
            this.setState({ uploadSuccessful: true });
          }
        })
        .catch(d => {
          console.log("Error saving the data");
          console.log(d);
          this.setState({ modalVisible: false });
        });
  };

  editUser = data => {
    // if (
    //   !window.confirm("Are you sure that you want add this user as portal user?")
    // )
    //   return false;
    let user_id = this.props.match.params.id;
    let postData = data;
    let addOnData = JSON.stringify({msisdn: postData.msisdn});
  
    postData.group_id = parseInt(postData.group_id);

    fetch(`${window.server}/admins/${user_id}`, {
      method: "PATCH",
      headers: {
        Authorization: localStorage.token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: postData.name,
        email: postData.email,
        group_id: postData.group_id,
        addOnData: addOnData,
        status: '1',
        secondFactor_status: postData.secondFactor_status,
        secondFactor_selected: postData.secondFactor_selected
      })
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.code) {
          alert(
            response.message +
              " \n " +
              (response.errors[0] ? response.errors[0].message : "")
          );
          this.setState({ addModal: false });
        } else {
          console.log(response);
          this.fetchAdmins();

          this.setState({ addModal: false });
        }
      })
      .catch(d => {
        console.log("Error saving the data");
        console.log(d);
        this.setState({ modalVisible: false });
      });
  };

  resetPass = data => {
    if (
      !window.confirm("Are you sure that you want to reset the password this user's account?")
    )
      return false;

    fetch(`${window.server}/admins/${this.state.id}`, {
      method: 'PATCH',
      headers: {
        Authorization: localStorage.token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        ACTION:'RESET_PIN',
        'email':this.state.email
      })
    })
        .then(response => response.json())
        .then(response => {
          if (response.code) {
            window.alert2.show({
              loader: false,
              icon: <XCircle size={60}></XCircle>,
              title: 'Error ' + response.code,
              buttons: true,
              message:
                  response.message +
                  ' \n ' +
                  (response.errors[0] ? response.errors[0].message : '')
            });

            this.setState({ modalVisible: false });
          } else {
            window.alert2.show({
              loader: false,
              icon: <CheckCircle size={60}></CheckCircle>,
              title: 'Request Sent Successfully.Await an Email',
              buttons: true
            });
          }
        })
        .catch(d => {
          //console.log("Error saving the data");
          //console.log(d);
          this.setState({ modalVisible: false });
        });


  };

}

export default PortalView;
